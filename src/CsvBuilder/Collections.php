<?php

namespace Drupal\rfp_csv_builder\CsvBuilder;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Database\Database;

/**
 * Csv builder for collections of tracks.
 */
class Collections {

  use LoggerChannelTrait;

  /**
   * Build databasae connection.
   */
  public function build() {

    $this->writeCollections($this->fetchCollections());
  }

  /**
   * Fetch all collections.
   */
  public function fetchCollections() {

    $collections = [];

    // Switch to external database.
    Database::setActiveConnection('migrate');
    $db = Database::getConnection();
    $query = $db->query("SELECT title, nid FROM node WHERE type='artist' ORDER BY title");
    $artists = $query->fetchAll();

    foreach ($artists as $artist) {

      // Get recordings for this artist.
      $recordingsQuery = $db->query("SELECT * FROM `field_data_field_artists`
            LEFT JOIN node ON node.nid = field_data_field_artists.entity_id
            LEFT JOIN field_data_field_album_art ON field_data_field_album_art.entity_id = field_data_field_artists.entity_id
            LEFT JOIN file_managed ON file_managed.fid = field_data_field_album_art.field_album_art_fid
            LEFT JOIN field_data_field_year_of_release
            ON field_data_field_year_of_release.entity_id = field_data_field_artists.entity_id
            WHERE `field_artists_target_id` = '{$artist->nid}'
            AND field_data_field_artists.bundle='recording' ");
      $recordings = $recordingsQuery->fetchAll();
      foreach ($recordings as $recording) {

        $artStub = $recording->filename;
        if (trim($artStub) == '') {
          $artStub = 'NOART.jpg';
        }

        $artUrl = drupal_get_path('module', 'rfp_csv_builder') . '/assets/covers/' . $artStub;
        $year = $recording->field_year_of_release_value;
        $recordingData = [
          'collection_' . $recording->nid,
          utf8_decode($recording->title),
          $artUrl,
          $year,
          'artist_' . $artist->nid,
        ];

        array_push($collections, $recordingData);
      }
    }

    // Flip back to default db.
    Database::setActiveConnection();
    return $collections;
  }

  /**
   * Write collections.
   */
  public function writeCollections($data) {

    $targetDir = drupal_get_path('module', 'rfp_csv_builder') . '/csv/';
    $targetFile = $targetDir . 'collections.csv';
    $imagesTargetFile = $targetDir . 'collections_covers.csv';

    if (!file_exists($targetDir)) {
      mkdir($targetDir);
    }
    $fh = fopen($targetFile, 'w');
    $fhimg = fopen($imagesTargetFile, 'w');

    fputcsv($fh,
        ['field_call_number',
          'title',
          'cover_art',
          'year_of_release',
          'field_artists',
        ]);
    fputcsv($fhimg, ['field_call_number', 'filename']);

    foreach ($data as $fields) {
      fputcsv($fh, $fields);
      fputcsv($fhimg, [$fields[0], $fields[2]]);
    }
    fclose($fh);
    fclose($fhimg);

    \Drupal::logger('collections')->notice('Wrote ' . count($data) . ' collections to ' . $targetFile);
  }

}
