<?php

namespace Drupal\rfp_csv_builder\MigrationBuilder;

/**
 * Build CollectionCovers migration.
 */
class CollectionsCovers {

  /**
   * Build the Migrations.
   */
  public function buildMigration() {

    $this->buildMigrationForFiles();
    $this->buildMigrationForMedia();
  }

  /**
   * Build the Migration for the files.
   */
  public function buildMigrationForFiles() {

    $csv = DRUPAL_ROOT . '/'
        . drupal_get_path('module', 'rfp_csv_builder')
        . '/csv/collections.csv';

    if (!file_exists($csv)) {
      \Drupal::logger('rfp_csv_builder')->error("$csv does not exist");
      return NULL;
    }

    $targetFile = DRUPAL_ROOT . '/'
            . drupal_get_path('module', 'rfp_csv_builder')
            . '/migrations/collections_covers_migration_files.yml';

    $ymldata = '';
    $ymltemplate = $this->getYamlTemplateForFilesMigration();

    $fh = fopen($csv, 'r');
    while ($row = fgetcsv($fh)) {

      $callnum = $row[0];
      $img = $row[2];
      if ($callnum == 'field_call_number') {
        continue;
      }

      $ymldata .= '    -' . "\n";
      $ymldata .= '      field_call_number: ' . $callnum . "\n";
      $ymldata .= '      image: \'/canonical_local/collection_covers/' . $img . "'\n";
    }

    $yml = str_replace('DATA_ROWS_HERE', $ymldata, $ymltemplate);

    file_put_contents($targetFile, $yml);
    // Import our newly created migration.
    $config_path = drupal_get_path('module', 'rfp_csv_builder') . '/migrations/collections_covers_migration_files.yml';
    $raw = file_get_contents($config_path);
    $data = yaml_parse($raw);

    $config = \Drupal::service('config.factory')->getEditable('migrate_plus.migration.collections_cover_images_as_files');
    $config->setData($data)->save();
    \Drupal::logger('rfp_csv_builder')->notice('imported collections covers as files migration');
  }

  /**
   * Build th3e migrations for media.
   */
  public function buildMigrationForMedia() {

    $targetFile = DRUPAL_ROOT . '/'
    . drupal_get_path('module', 'rfp_csv_builder')
    . '/migrations/collections_covers_migration_media.yml';

    $ymldata = "\n";
    $ymltemplate = $this->getYamlTemplateForMediaMigration();
    // \Drupal::logger('rfp_csv_builder')->error("TPLO:" . $ymltemplate);
    // print $ymltemplate; exit();
    $csv = DRUPAL_ROOT . '/'
        . drupal_get_path('module', 'rfp_csv_builder')
        . '/csv/collections.csv';

    if (!file_exists($csv)) {
      \Drupal::logger('rfp_csv_builder')->error("$csv does not exist");
      return NULL;
    }

    $fh = fopen($csv, 'r');
    while ($row = fgetcsv($fh)) {

      $callnum = $row[0];
      $img = $row[2];
      if ($callnum == 'field_call_number') {
        continue;
      }

      $ymldata .= '    -' . "\n";
      $ymldata .= '      field_call_number: ' . $callnum . "\n";
      $ymldata .= '      image: \'/canonical_local/collection_covers/' . $img . "'\n";
    }

    $yml = str_replace('DATA_ROWS_HERE', $ymldata, $ymltemplate);

    file_put_contents($targetFile, $yml);

    $config_path = drupal_get_path('module', 'rfp_csv_builder') . '/migrations/collections_covers_migration_media.yml';
    $raw = file_get_contents($config_path);
    $data = yaml_parse($raw);

    $config = \Drupal::service('config.factory')->getEditable('migrate_plus.migration.collections_cover_images_as_media');
    $config->setData($data)->save();
    \Drupal::logger('rfp_csv_builder')->notice('imported collections covers as files migration');

  }

  /**
   * Get the YAML template for files migraiton.
   */
  private function getYamlTemplateForFilesMigration() {

    $tpl = <<<'END_TEMPLATE'
id: collections_cover_images_as_files
migration_group: default
migration_tags:
  - images
label: 'Import collection cover images (as files)'
source:
  plugin: embedded_data
  data_rows:

DATA_ROWS_HERE
  ids:
    field_call_number:
      type: string
  constants:
    DRUPAL_FILE_DIRECTORY: 'public://images/covers/'
process:
  pseudo_destination_filename:
    -
      plugin: callback
      callable: basename
      source: image
    -
      plugin: skip_on_empty
      method: row
      message: 'Cannot import empty image filename.'
  pseudo_destination_path:
    -
      plugin: concat
      source:
        - constants/DRUPAL_FILE_DIRECTORY
        - '@pseudo_destination_filename'
  uri:
    -
      plugin: skip_on_empty
      method: process
      source: image
    -
      plugin: file_copy
      source:
        - image
        - '@pseudo_destination_path'
      file_exists: rename
      move: false
destination:
  plugin: 'entity:file'
migration_dependencies: null


END_TEMPLATE;

    return $tpl;
  }

  /**
   * Get the YAML template for media migration.
   */
  private function getYamlTemplateForMediaMigration() {

    $tpl = <<<'END_TEMPLATE'
      id: collections_cover_images_as_media
      migration_group: default
      migration_tags:
        - images
      label: 'Migrate images to media'
      source:
        plugin: embedded_data
        data_rows:

      DATA_ROWS_HERE
        ids:
          field_call_number:
            type: string
      process:
        pseudo_destination_filename:
          -
            plugin: callback
            callable: basename
            source: image
          -
            plugin: skip_on_empty
            method: row
            message: 'Cannot import empty filename.'
        pseudo_destination_path:
          -
            plugin: concat
            source:
              - '@pseudo_destination_filename'
        name: '@pseudo_destination_filename'
        field_cover_art/target_id:
          plugin: migration_lookup
          migration: collections_cover_images_as_files
          source: field_call_number
        
      destination:
        plugin: 'entity:media'
        default_bundle: image
      

      END_TEMPLATE;

    return $tpl;
  }

}
