<?php

namespace Drupal\rfp_csv_builder\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\rfp_csv_builder\CsvBuilder\Artists;
use Drupal\rfp_csv_builder\CsvBuilder\Collections;
use Drupal\rfp_csv_builder\CsvBuilder\Tracks;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class RfpCsvBuilderCommands extends DrushCommands {

  use LoggerChannelTrait;

  /**
   * The database connection.
   *
   * @var connString
   */
  private $conn;

  /**
   * The module installer.
   *
   * @var installerDrupal\Core\Extension\ModuleInstaller
   */
  private $installer;

  /**
   * Build the rfp csv files from the lcmp database.
   *
   * @option option-name
   *   Description
   * @usage rfp_csv_builder-buildRfpCsv
   *   Usage description
   *
   * @command rfp_csv_builder:buildRfpCsv
   * @aliases rfpbuild rfpcsv
   */
  public function buildRfpCsv() {

    $this->cleanOutFiles();
    $this->getLogger('rfp_csv')->notice('Preparing to write csv data');

    $a = new Artists();
    $a->build();

    $c = new Collections();
    $c->build();

    $t = new Tracks();
    $t->build();
  }

  /**
   * Clean out generated files.
   */
  private function cleanOutFiles() {

    $delete = [
      DRUPAL_ROOT . '/' . drupal_get_path('module', 'rfp_csv_builder') . '/csv/artists.csv',
      DRUPAL_ROOT . '/' . drupal_get_path('module', 'rfp_csv_builder') . '/csv/collections.csv',
      DRUPAL_ROOT . '/' . drupal_get_path('module', 'rfp_csv_builder') . '/csv/tracks.csv',
    ];

    foreach ($delete as $path) {

      if (file_exists($path)) {
        unlink($path);
      }
    }
  }

}
